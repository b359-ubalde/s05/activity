# 1. Create a Person class that is an abstract class that has the following methods
#   a. getFullName method
#   b. addRequest method
#   c. checkRequest method
#   d. addUser method

from abc import ABC, abstractclassmethod


class Person(ABC):
    # getFullName method
    @abstractclassmethod
    def getFullName(self):
        pass

    # addRequest method
    @abstractclassmethod
    def addRequest(self):
        pass

    # checkRequest method
    @abstractclassmethod
    def checkRequest(self):
        pass

    # addUser method
    @abstractclassmethod
    def addUser(self):
        pass


# 2. Create an Employee class from Person with the following properties and methods:
#   a. Properties (make sure they are private and have getters/setters)
#       firstName, lastName, email, department
#   b. Methods
# Abstract methods
#       For the checkRequest and addUser methods, they should do nothing.
# Custom methods
#       login() - outputs "<Email> has logged in"
#       logout() - outputs "<Email> has logged out"
#       Note: All methods just return Strings of simple text
#           ex. Request has been added

class Employee(Person):
    def __init__(self, firstName, lastName, email, department):
        super().__init__()
        self._firstName = firstName
        self._lastName = lastName
        self._email = email
        self._department = department

    # set_firstName
    def set_firstName(self, firstName):
        self._firstName = firstName

    # set_lastName
    def set_lastName(self, lastName):
        self._lastName = lastName

    # set_email
    def set_email(self, email):
        self._email = email

    # set_department
    def set_department(self, department):
        self._department = department

    # get_firstName
    def get_firstName(self):
        return self._firstName

    # get_lastName
    def get_lastName(self):
        return self._lastName

    # get_email
    def get_email(self):
        return self._email

    # get_department
    def get_department(self):
        return self._department

    def getFullName(self):
        return (self._firstName + " " + self._lastName)

    def addRequest(self):
        return ("Request has been added")

    def checkRequest(self):
        pass

    def addUser(self):
        pass

    def login(self):
        return (f"{self._email} has logged in")

    def logout(self):
        return (f"{self._email} has logged out")


# 3. Create a TeamLead class from Person with the following properties and methods
#   a. Properties (make sure they are private and have getters/setters)
#       firstName, lastName, email, department, members - []
#   b. Methods
# Abstract methods
#       For the addRequest and addUser methods, they should do nothing.
# Custom methods
#       login() - outputs "<Email> has logged in"
#       logout() - outputs "<Email> has logged out"
#       addMember() - adds an employee to the members list
#       Note: All methods just return Strings of simple text
#           ex. Request has been added

class TeamLead(Person):
    def __init__(self, firstName, lastName, email, department):
        super().__init__()
        self._firstName = firstName
        self._lastName = lastName
        self._email = email
        self._department = department
        self.members = []

    # set_firstName
    def set_firstName(self, firstName):
        self._firstName = firstName

    # set_lastName
    def set_lastName(self, lastName):
        self._lastName = lastName

    # set_email
    def set_email(self, email):
        self._email = email

    # set_department
    def set_department(self, department):
        self._department = department

    # get_firstName
    def get_firstName(self):
        return self._firstName

    # get_lastName
    def get_lastName(self):
        return self._lastName

    # get_email
    def get_email(self):
        return self._email

    # get_department
    def get_department(self):
        return self._department

    # get_members
    def get_members(self):
        return self.members

    def getFullName(self):
        return (f"{self._firstName} {self._lastName}")

    def addRequest(self):
        pass

    def checkRequest(self):
        return ("Employee check request")

    def addUser(self):
        pass

    def login(self):
        return (f"{self._email} has logged in")

    def logout(self):
        return (f"{self._email} has logged out")

    def addMember(self, employeeId):
        self.members.append(employeeId)


# 4. Create an Admin class from Person with the following properties and methods
#   a. Properties(make sure they are private and have getters/setters)
#       firstName, lastName, email, department
#   b. Methods
# Abstract methods
#       For the checkRequest and addRequest methods, they should do nothing.
# Custom methods
#       login() - outputs "<Email> has logged in"
#       logout() - outputs "<Email> has logged out"
#       addUser() - outputs "New user added"
#       Note: All methods just return Strings of simple text
#           ex. Request has been added

class Admin(Person):
    def __init__(self, firstName, lastName, email, department):
        super().__init__()
        self._firstName = firstName
        self._lastName = lastName
        self._email = email
        self._department = department

    # set_firstName
    def set_firstName(self, firstName):
        self._firstName = firstName

    # set_lastName
    def set_lastName(self, lastName):
        self._lastName = lastName

    # set_email
    def set_email(self, email):
        self._email = email

    # set_department
    def set_department(self, department):
        self._department = department

    # get_firstName
    def get_firstName(self):
        return self._firstName

    # get_lastName
    def get_lastName(self):
        return self._lastName

    # get_email
    def get_email(self):
        return self._email

    # get_department
    def get_department(self):
        return self._department

    def getFullName(self):
        return (f"{self._firstName} {self._lastName}")

    def addRequest(self):
        pass

    def checkRequest(self):
        pass

    def addUser(self):
        return ("User has been added")

    def login(self):
        return (f"{self._email} has logged in")

    def logout(self):
        return (f"{self._email} has logged out")

    def addMember():
        return ("New User Added")

# 5. Create a Request Class that has the following properties and methods
#   a. properties
#       name, requester, dateRequested, status
#   b. Methods
#       updateRequest
#       closeRequest
#       cancelRequest
#       Note: All methods just return Strings of simple text
#           Ex. Request < name > has been updated/closed/cancelled


class Request():
    def __init__(self, name, requester, dateRequested):
        self._name = name
        self._requester = requester
        self._dateRequested = dateRequested
        self.status = "None"
        # self._status = status

    # set_name
    def set_name(self, name):
        self._name = name

    # set_requester
    def set_requester(self, requester):
        self._requester = requester

    # set_dateRequested
    def set_dateRequested(self, dateRequested):
        self._dateRequested = dateRequested

    # set_status
    def set_status(self, status):
        self.status = status

    # get_name
    def get_name(self):
        return self._name

    # get_requester
    def get_requester(self):
        return self._requester

    # get_dateRequested
    def get_dateRequested(self):
        return self._dateRequested

    # get_status
    def get_status(self):
        return self.status

    # methods
    def updateRequest(self):
        return (f"Request {self._name} has been updated")

    def closeRequest(self):
        return (f"Request {self._name} has been closed")

    def cancelRequest(self):
        return (f"Request {self._name} has been cancelled")


# Test cases:
employee1 = Employee("John", "Doe", "djohn@mail.com", "Marketing")
employee2 = Employee("Jane", "Smith", "sjane@mail.com", "Marketing")
employee3 = Employee("Robert", "Patterson", "probert@mail.com", "Sales")
employee4 = Employee("Brandon", "Smith", "sbrandon@mail.com", "Sales")
admin1 = Admin("Monika", "Justin", "jmonika@mail.com", "Marketing")
teamLead1 = TeamLead("Michael", "Specter", "smichael@mail.com", "Sales")
req1 = Request("New hire orientation", teamLead1, "27-Jul-2021")
req2 = Request("Laptop repair", employee1, "1-Jul-2021")

assert employee1.getFullName() == "John Doe", "Full name should be John Doe"
assert admin1.getFullName() == "Monika Justin", "Full name should be Monika Justin"
assert teamLead1.getFullName() == "Michael Specter", "Full name should be Michael Specter"
assert employee2.login() == "sjane@mail.com has logged in"
assert employee2.addRequest() == "Request has been added"
assert employee2.logout() == "sjane@mail.com has logged out"

teamLead1.addMember(employee3)
teamLead1.addMember(employee4)
for indiv_emp in teamLead1.get_members():
    print(indiv_emp.getFullName())

assert admin1.addUser() == "User has been added"

req2.set_status("closed")
print(req2.closeRequest())
